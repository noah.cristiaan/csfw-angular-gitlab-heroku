import { HttpClient } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { Observable, tap } from 'rxjs'
import { PokemonItem } from './items.model'

@Injectable({
  providedIn: 'root'
})
export class ItemService {
  APIString: String = 'https://pokenodemon-api.herokuapp.com/'
  // APIString: String = 'http://localhost:8080/'

  constructor(public readonly http: HttpClient) {}

  Get(): Observable<PokemonItem[]> {
    return this.http.get(this.APIString + 'items').pipe(tap(console.log))
  }

  GetById(id: String): Observable<PokemonItem> {
    return this.http.get(this.APIString + 'items/' + id).pipe(tap(console.log))
  }

  Add(item: PokemonItem): Observable<PokemonItem> {
    console.log(item)
    return this.http.post<any>(this.APIString + 'items', item).pipe(tap(console.log))
  }

  Update(item: PokemonItem): Observable<PokemonItem> {
    return this.http.put<PokemonItem>(this.APIString + 'items/' + item._id, item).pipe(tap(console.log))
  }

  Delete(item: PokemonItem, itemId: String) {
    return this.http
      .delete<any>(this.APIString.toString() + 'items/' + itemId)
      .pipe(tap(console.log))
      .subscribe()
  }
}
