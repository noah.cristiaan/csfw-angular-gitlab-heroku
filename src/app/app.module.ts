import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
import { RouterModule } from '@angular/router'
import { ReactiveFormsModule } from '@angular/forms'
import { AppRoutingModule } from './app-routing.module'
import { AppComponent } from './app.component'
import { UserListComponent } from './core/user/user-list/user-list.component'
import { DashboardComponent } from './core/dashboard/dashboard.component'
import { NavbarComponent } from './core/navbar/navbar.component'
import { LayoutComponent } from './core/layout/layout.component'
import { FooterComponent } from './core/footer/footer.component'
import { AboutComponent } from './pages/about/about.component'
import { UsecaseComponent } from './pages/about/usecases/usecase.component'
import { UserDetailComponent } from './core/user/user-detail/user-detail.component'
import { UserEditComponent } from './core/user/user-edit/user-edit.component'
import { UserDefaultComponent } from './core/user/user-default/user-default.component'
import { UserRegisterComponent } from './core/user/user-register/user-register.component'
import { PokedexComponent } from './pages/pokedex/pokedex.component'
import { PokemonTrainerComponent } from './pages/pokemon-trainer/pokemon-trainer.component'
import { PokemonTrainerDashboardComponent } from './pages/pokemon-trainer/pokemon-trainer-dashboard/pokemon-trainer-dashboard.component'
import { MovesComponent } from './pages/moves/moves-modal/moves.modal.component'
import { MovesSelectorComponent } from './pages/moves/moves-selector/moves.selector.component'
import { HttpClientModule } from '@angular/common/http'
import { PokemonComponent } from './pages/pokemon/pokemon.component'
import { TypeImageSelectorComponent } from './pages/type/type-image-selector.component'
import { PokedexAddComponent } from './pages/pokedex/pokedex-add/pokedex-add.component'
import { PokedexAddComponentModal } from './pages/pokedex/pokedex-add/pokedex-add-modal/pokedex-add.modal.component'
import { UserLoginComponent } from './core/user/user-login/user-login.component'
import { MovesAddComponent } from './pages/moves/moves-add/moves-add.component'
import { MovesAddComponentModal } from './pages/moves/moves-add/moves-add-modal/moves-add.modal.component'
import { ItemAddComponent } from './pages/item/item-add/item-add.component'
import { ItemAddComponentModal } from './pages/item/item-add/item-add-modal/item-add.modal.component'
import { ItemSelectorComponent } from './pages/item/item-selector/item.selector.component'
import { UserFollowingComponent } from './core/user/user-following/user-following.component'
import { RouterTestingModule } from '@angular/router/testing'

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    LayoutComponent,
    DashboardComponent,
    FooterComponent,
    AboutComponent,
    UsecaseComponent,
    UserListComponent,
    UserDetailComponent,
    UserEditComponent,
    UserDefaultComponent,
    UserRegisterComponent,
    UserLoginComponent,
    PokedexComponent,
    PokemonTrainerComponent,
    PokemonComponent,
    PokemonTrainerDashboardComponent,
    MovesComponent,
    MovesSelectorComponent,
    TypeImageSelectorComponent,
    PokedexAddComponent,
    PokedexAddComponentModal,
    UserRegisterComponent,
    MovesAddComponent,
    MovesAddComponentModal,
    ItemAddComponent,
    ItemAddComponentModal,
    ItemSelectorComponent,
    UserFollowingComponent
  ],
  imports: [
    BrowserModule,
    RouterModule,
    NgbModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterTestingModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
