import { HttpClientTestingModule } from '@angular/common/http/testing'
import { ComponentFixture, TestBed } from '@angular/core/testing'
import { RouterTestingModule } from '@angular/router/testing'
import { Observable, of } from 'rxjs'
import { PokemonMoveService } from 'src/app/core/moves/moves.service'
import { User, UserGender, UserRole } from 'src/app/core/user/user.model'
import * as moment from 'moment'
import { PokemonMove, StatusEffect } from 'src/app/core/moves/moves.model'
import { UserService } from 'src/app/core/user/user.service'
import { Pokemon, PokemonType } from '../../pokemon/pokemon.model'
import { MovesSelectorComponent } from './moves.selector.component'
import { PokemonService } from '../../pokemon/pokemon.service'

const expectedMove: PokemonMove[] = [
  {
    _id: '1',
    name: 'Fly',
    statusEffect: StatusEffect.NONE,
    type: PokemonType.FLYING,
    accuracy: 95,
    damage: 90
  }
]

const expectedUserWithPokemonTeam: User[] = [
  {
    _id: '1',
    birthdate: moment().toDate(),
    password: 'secret',
    firstname: 'Noah',
    lastname: 'de Keijzer',
    emailaddress: 'noah.cristiaan@gmail.com',
    userGender: UserGender.Male,
    userRole: UserRole.Guest,
    badges: 0,
    pokemonTeam: [
      {
        _id: '1',
        pokemonImageURL: 'https://assets.pokemon.com/assets/cms2/img/pokedex/detail/006.png',
        name: 'Charizard ',
        types: [PokemonType.FIRE, PokemonType.FLYING],
        moves: [],
        nature: 'Brave',
        ability: 'Blaze',
        item: undefined
      }
    ]
  }
]
const expectedUserWithPokemonTeamAndOneMove: User[] = [
    {
      _id: '1',
      birthdate: moment().toDate(),
      password: 'secret',
      firstname: 'Noah',
      lastname: 'de Keijzer',
      emailaddress: 'noah.cristiaan@gmail.com',
      userGender: UserGender.Male,
      userRole: UserRole.Guest,
      badges: 0,
      pokemonTeam: [
        {
          _id: '1',
          pokemonImageURL: 'https://assets.pokemon.com/assets/cms2/img/pokedex/detail/006.png',
          name: 'Charizard ',
          types: [PokemonType.FIRE, PokemonType.FLYING],
          moves: ['1'],
          nature: 'Brave',
          ability: 'Blaze',
          item: undefined
        }
      ]
    }
  ]

  
const expectedPokemonsWithItemAndMove: Pokemon[] = [
  {
    _id: '1',
    pokemonImageURL: 'https://assets.pokemon.com/assets/cms2/img/pokedex/detail/006.png',
    name: 'Charizard ',
    types: [PokemonType.FIRE, PokemonType.FLYING],
    moves: ['1'],
    nature: 'Brave',
    ability: 'Blaze',
    item: undefined
  }
]
//describe to start tests
describe('Update Moveset of pokemon in the pokemon team of the user', () => {
  //mock variables used in tests
  
  let component: MovesSelectorComponent
  let userServiceSpy: jasmine.SpyObj<UserService>
  let pokemonServiceSpy: jasmine.SpyObj<PokemonService>
  let pokemonMoveServiceSpy: jasmine.SpyObj<PokemonMoveService>
  
  beforeEach(() => {
    //create mock from httpclient
    userServiceSpy = jasmine.createSpyObj('UserService', [
      'Get',
      'GetById',
      'Update',
      'GetUserLogedIn',
      'SetUserLogedIn'
    ])
    pokemonServiceSpy = jasmine.createSpyObj('PokemonService', ['Get','GetById', 'GetCurrentPokemon', 'SetCurrentPokemon'])
    pokemonMoveServiceSpy = jasmine.createSpyObj('PokemonMoveService', ['Get', 'GetById'])

    //configure object under test
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule],
      providers: [
        { provide: PokemonService, useValue: pokemonServiceSpy },
        { provide: UserService, useValue: userServiceSpy },
        { provide: PokemonMoveService, useValue: pokemonMoveServiceSpy }
      ]
    }).compileComponents()
    //create object under test
    component = TestBed.createComponent(MovesSelectorComponent).componentInstance

    //di for object under test
    userServiceSpy = TestBed.inject(UserService) as jasmine.SpyObj<UserService>
    pokemonServiceSpy = TestBed.inject(PokemonService) as jasmine.SpyObj<PokemonService>
    pokemonMoveServiceSpy = TestBed.inject(PokemonMoveService) as jasmine.SpyObj<PokemonMoveService>

    pokemonServiceSpy.currentPokemon = expectedUserWithPokemonTeam[0].pokemonTeam[0]
    userServiceSpy.userLogedIn = expectedUserWithPokemonTeam[0]
    component.moves = expectedMove
    component.userLogedIn = expectedUserWithPokemonTeam[0]
    component.pokemon = expectedUserWithPokemonTeam[0].pokemonTeam[0]
  })

  //test if service is created
  it('should be created', () => {
    //expect service to exist
    expect(component).toBeTruthy()
  })

  it('should have correct data', () => {
    component.ngOnInit()
    expect(component.userLogedIn?.pokemonTeam!.length).toBeGreaterThan(0)
  })

  it('should add move to pokemon in pokemon team', () => {
    component.ngOnInit()
    userServiceSpy.Update.and.returnValue(of(expectedUserWithPokemonTeamAndOneMove[0]))
    pokemonMoveServiceSpy.Get.and.returnValue(of(expectedMove))
    pokemonServiceSpy.GetById.and.returnValue(of(expectedUserWithPokemonTeam[0].pokemonTeam[0]))
    component.AddMove(expectedMove[0]._id)
    expect(component.userLogedIn?.pokemonTeam![0].moves[0]).toEqual(expectedMove[0]._id)
  })
})
