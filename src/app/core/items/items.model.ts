export class PokemonItem {
  _id: string
  name: string
  description: string
  imageURL: string

  constructor(_id: string, name: string, description: string, imageURL: string) {
    this._id = _id
    this.name = name
    this.description = description
    this.imageURL = imageURL
  }
}
