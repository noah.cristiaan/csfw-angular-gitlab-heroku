import { Pokemon } from 'src/app/pages/pokemon/pokemon.model'

const mongoose = require('mongoose')
const Schema = mongoose.Schema

export enum UserRole {
  Guest,
  Editor,
  Admin
}

export enum UserGender {
  Male,
  Female,
  Other,
  None
}

export class User {
  _id: String
  birthdate: Date = new Date()
  password: string = ''
  firstname: string = ''
  lastname: string = ''
  emailaddress: string = ''
  userRole: UserRole = UserRole.Guest
  userGender: UserGender = UserGender.Male
  pokemonTeam!: Pokemon[]
  badges!: Number

  constructor(
    _id: String,
    firstname: string = '',
    lastname: string = '',
    emailaddress: string = '',
    birthdate: Date = new Date(),
    userGender: UserGender,
    password: string
  ) {
    this._id = _id
    this.emailaddress = emailaddress
    this.firstname = firstname
    this.lastname = lastname
    this.birthdate = birthdate
    this.userGender = userGender
    this.password = password
    this.pokemonTeam = []
    this.pokemonTeam.length = 6
    this.badges = 0
  }
}
