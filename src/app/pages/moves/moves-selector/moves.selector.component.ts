import { Component, Injectable, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { NgbModal } from '@ng-bootstrap/ng-bootstrap'
import { PokemonMove } from 'src/app/core/moves/moves.model'
import { PokemonMoveService } from 'src/app/core/moves/moves.service'
import { User } from 'src/app/core/user/user.model'
import { UserService } from 'src/app/core/user/user.service'
import { TypeImageSelectorComponent } from 'src/app/pages/type/type-image-selector.component'
import { environment } from 'src/environments/environment'
import { Pokemon, PokemonType } from '../../pokemon/pokemon.model'
import { PokemonService } from '../../pokemon/pokemon.service'

@Component({
  selector: 'move-selector',
  templateUrl: './moves.selector.component.html'
})
@Injectable({
  providedIn: 'root'
})
export class MovesSelectorComponent implements OnInit {
  runningMode: string = ''
  apiUrl: string = ''
  version: string = ''
  moves!: PokemonMove[]
  pokemonType = PokemonType
  typeImageSelector: TypeImageSelectorComponent
  userLogedIn!: User | undefined
  pokemon!: Pokemon

  constructor(
    private router: Router,
    private userService: UserService,
    private pokemonService: PokemonService,
    private modalService: NgbModal,
    private pokemonMoveService: PokemonMoveService,
    typeImageSelector: TypeImageSelectorComponent
  ) {
    this.pokemonMoveService = pokemonMoveService
    this.typeImageSelector = typeImageSelector
  }

  ngOnInit() {
    this.runningMode = environment.production ? 'production' : 'development'
    this.apiUrl = environment.apiUrl
    this.version = environment.version
    if (!this.userLogedIn) {
      this.GetUserLogedIn()
    }
    if (!this.pokemon) {
      this.GetCurrentPokemon()
    }
    if (!this.moves) {
      this.GetAllMoves()
    }
  }

  GetAllMoves() {
    this.pokemonMoveService.Get().subscribe((results) => {
      this.moves = results
    })
  }

  GetCurrentPokemon() {
    this.pokemon = this.pokemonService.GetCurrentPokemon()
  }
  GetUserLogedIn() {
    this.userLogedIn = this.userService.GetUserLogedIn()
  }

  AddMove(_idMove: String) {
      let pokemon = this.GetPokemonInTeam()
      pokemon!.moves.push(_idMove)
      this.UpdateUserPokemonTeam(pokemon)
  }

  GetPokemonInTeam(): Pokemon {
    var pokemonToAddMoveTo = this.userLogedIn!.pokemonTeam!.find(
      (pokemon) => pokemon._id === this.pokemon._id
    )
    return pokemonToAddMoveTo!
  }

  UpdateUserPokemonTeam(pokemon: Pokemon) {
    this.userService.Update(this.userLogedIn!).subscribe(() => {
      this.pokemonService.SetCurrentPokemon(pokemon!)
      this.modalService.dismissAll()
      this.UpdateComponent()
    })
  }
  UpdateComponent() {
    let currentUrl = this.router.url
    this.router.routeReuseStrategy.shouldReuseRoute = () => false
    this.router.onSameUrlNavigation = 'reload'
    this.router.navigate([currentUrl])
  }
}
