import { PokemonItem } from 'src/app/core/items/items.model'
import { PokemonMove } from 'src/app/core/moves/moves.model'

export enum PokemonType {
  NORMAL = 'NORMAL',
  FIRE = 'FIRE',
  WATER = 'WATER',
  ELETRIC = 'ELETRIC',
  GRASS = 'GRASS',
  ICE = 'ICE',
  FIGHTING = 'FIGHTING',
  POISON = 'POISON',
  GROUND = 'GROUND',
  FLYING = 'FLYING',
  PSYCHIC = 'PSYCHIC',
  BUG = 'BUG',
  ROCK = 'ROCK',
  GHOST = 'GHOST',
  DRAGON = 'DRAGON',
  DARK = 'DARK',
  STEEL = 'STEEL',
  FAIRY = 'FAIRY'
}

export class Pokemon {
  _id!: String | undefined
  pokemonImageURL: String
  name: string
  nature: String
  ability: String
  types!: PokemonType[]
  moves!: String[]
  item!: String | undefined

  constructor(_id: String, name: string, types: PokemonType[], nature: String, ability: String, pokemonImageURL: String, moves: String[]) {
    this._id = _id
    this.pokemonImageURL = pokemonImageURL
    this.name = name
    this.types = types
    this.moves = moves
    this.nature = nature
    this.ability = ability
  }
}
