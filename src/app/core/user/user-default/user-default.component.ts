import { Component, OnInit } from '@angular/core'
import { environment } from 'src/environments/environment'

@Component({
  selector: 'user-default',
  templateUrl: './user-default.component.html'
})
export class UserDefaultComponent implements OnInit {
  runningMode: string = ''
  apiUrl: string = ''
  version: string = ''

  constructor() {}

  ngOnInit() {
    this.runningMode = environment.production ? 'production' : 'development'
    this.apiUrl = environment.apiUrl
    this.version = environment.version
  }
}
