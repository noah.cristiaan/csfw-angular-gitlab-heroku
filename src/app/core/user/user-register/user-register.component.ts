import { Component, OnInit } from '@angular/core'
import { environment } from 'src/environments/environment'
import { UserService } from '../user.service'
import { ActivatedRoute, Router } from '@angular/router'
import { FormControl, FormGroup, Validators } from '@angular/forms'
import { rejects } from 'assert'
import { User } from '../user.model'
import { UserListComponent } from '../user-list/user-list.component'

@Component({
  selector: 'user-register',
  templateUrl: './user-register.component.html'
})
export class UserRegisterComponent implements OnInit {
  runningMode: string = ''
  apiUrl: string = ''
  version: string = ''
  user!: User
  registerUserForm!: FormGroup

  constructor(
    private route: ActivatedRoute,
    private userService: UserService,
    private router: Router
  ) {}

  ngOnInit() {
    this.runningMode = environment.production ? 'production' : 'development'
    this.apiUrl = environment.apiUrl
    this.version = environment.version

    this.registerUserForm = new FormGroup({
      firstname: new FormControl('', [Validators.required, Validators.minLength(2)]),
      lastname: new FormControl('', [Validators.required, Validators.minLength(2)]),
      password: new FormControl('', Validators.required),
      emailaddress: new FormControl('', [Validators.email, Validators.required]),
      birthdate: new FormControl('', Validators.required),
      userGender: new FormControl('', Validators.required)
    })
  }
  async onSubmit() {
    // Check if form is invalid
    // console.log('onSubmit', this.registerUserForm.value)
    if (this.registerUserForm.invalid) {
      return
    }
    var userAdd: User

    // Check if user exists
    await this.userService
      .GetByEmail(this.registerUserForm.controls['emailaddress'].value)
      .subscribe((result) => (userAdd = result))

    if (userAdd!) {
      this.registerUserForm.reset
      return
    } else {
      await this.userService.Add(this.registerUserForm.value!).subscribe((result: any) => {
        this.userService.GetById(result.user._id).subscribe((result) => {
          let currentUrl = this.router.url
          this.router.routeReuseStrategy.shouldReuseRoute = () => false
          this.router.onSameUrlNavigation = 'reload'
          this.router.navigate([currentUrl])
        })
      })
      this.registerUserForm.reset
    }
  }
}
