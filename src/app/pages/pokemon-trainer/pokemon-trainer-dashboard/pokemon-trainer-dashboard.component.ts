import { Component, OnInit } from '@angular/core'
import { environment } from '../../../../environments/environment'

@Component({
  selector: 'app-trainer-dashboard',
  templateUrl: './pokemon-trainer-dashboard.component.html'
})

export class PokemonTrainerDashboardComponent implements OnInit {
  runningMode: string = ''
  apiUrl: string = ''
  version: string = ''
  constructor() {}

  ngOnInit() {
    this.runningMode = environment.production ? 'production' : 'development'
    this.apiUrl = environment.apiUrl
    this.version = environment.version
  }
}
