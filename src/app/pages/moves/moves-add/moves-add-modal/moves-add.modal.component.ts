import { Component, Injectable, OnInit, ViewEncapsulation } from '@angular/core'
import { NgbModal } from '@ng-bootstrap/ng-bootstrap'
import { environment } from 'src/environments/environment'
import { PokemonService } from '../../../pokemon/pokemon.service'
import { MovesAddComponent } from '../moves-add.component'

@Component({
  selector: 'moves-add',
  templateUrl: './moves-add.modal.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: [
    `
      .dark-modal .modal-content {
        background-color: #292b2c;
        color: white;
      }
      .dark-modal .close {
        color: white;
      }
      .light-blue-backdrop {
        background-color: #5cb3fd;
      }
    `
  ]
})
export class MovesAddComponentModal implements OnInit {
  runningMode: string = ''
  apiUrl: string = ''
  version: string = ''

  constructor(
    private modalService: NgbModal,
    private pokemonService: PokemonService,
  ) {}

  ngOnInit() {
    this.runningMode = environment.production ? 'production' : 'development'
    this.apiUrl = environment.apiUrl
    this.version = environment.version
    this.openLg()
  }

  openLg() {
    this.modalService.open(MovesAddComponent, { size: 'lg' })
  }

  close(){
    this.modalService.dismissAll()
  }
}
