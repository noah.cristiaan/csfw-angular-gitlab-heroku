import { Component, Injectable, OnInit, ViewEncapsulation } from '@angular/core'
import { NgbModal } from '@ng-bootstrap/ng-bootstrap'
import { environment } from 'src/environments/environment'
import { Pokemon, PokemonType } from '../../pokemon/pokemon.model'
import { PokemonService } from '../../pokemon/pokemon.service'
import { ItemSelectorComponent } from '../item-selector/item.selector.component'

@Component({
  selector: 'item',
  templateUrl: './item.modal.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: [
    `
      .dark-modal .modal-content {
        background-color: #292b2c;
        color: white;
      }
      .dark-modal .close {
        color: white;
      }
      .light-blue-backdrop {
        background-color: #5cb3fd;
      }
    `
  ]
})
@Injectable({
  providedIn: 'root'
})
export class ItemComponent implements OnInit {
  pokemon!: Pokemon
  runningMode: string = ''
  apiUrl: string = ''
  version: string = ''

  constructor(private modalService: NgbModal, private itemSelectorComponent: ItemSelectorComponent, private pokemonService: PokemonService) {}

  ngOnInit() {
    this.runningMode = environment.production ? 'production' : 'development'
    this.apiUrl = environment.apiUrl
    this.version = environment.version
  }

  openLg(pokemon: Pokemon) {
    this.pokemonService.SetCurrentPokemon(pokemon)
    this.modalService.open(ItemSelectorComponent, { size: 'lg'})
  }
}
