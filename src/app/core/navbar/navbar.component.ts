import { Component, Input, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { User } from '../user/user.model'
import { UserService } from '../user/user.service'

@Component({
  selector: 'app-navbar',
  template: `
    <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
      <div class="container">
        <a
          class="navbar-brand"
          routerLink="/"
          [routerLinkActive]="['active']"
          [routerLinkActiveOptions]="{ exact: true }"
          >{{ title }}</a
        >
        <button
          class="navbar-toggler hidden-sm-up"
          type="button"
          data-target="#navbarsDefault"
          aria-controls="navbarsDefault"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarsDefault">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item">
              <a class="nav-link" routerLink="/pokedex" [routerLinkActive]="['active']">Pokedex</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" routerLink="/trainer" [routerLinkActive]="['active']">Trainer</a>
            </li>
            <li class="nav-item">
              <a
                class="nav-link"
                routerLink="about"
                [routerLinkActive]="['active']"
                [routerLinkActiveOptions]="{ exact: true }"
                >About</a
              >
            </li>
          </ul>
        </div>
        <div>
          <ul class="navbar-nav mr-auto">
            <li class="nav-item">
              <a
                *ngIf="userLogedIn === undefined"
                class="nav-link"
                routerLink="register"
                [routerLinkActive]="['register']"
                [routerLinkActiveOptions]="{ exact: true }"
                >Register</a
              >
            </li>
            <li class="nav-item">
              <a
                *ngIf="userLogedIn === undefined"
                class="nav-link"
                routerLink="login"
                [routerLinkActive]="['login']"
                [routerLinkActiveOptions]="{ exact: true }"
                >Login</a
              >
            </li>
            <li class="nav-item">
              <a
                *ngIf="userLogedIn !== undefined"
                class="nav-item"
                [routerLinkActive]="['logout']"
                (click)="Logout()"
                >Logout</a
              >
            </li>
          </ul>
        </div>
      </div>
    </nav>
  `,
  styles: [
    '.btn-link { color: rgba(255,255,255,.5); text-decoration: none; }',
    // tslint:disable-next-line: max-line-length
    '.btn-link.focus, .btn-link:focus, .btn-link.hover, .btn-link:hover { color: rgba(255,255,255,.75); text-decoration: none; box-shadow: none; }'
  ]
})
export class NavbarComponent {
  @Input() title: string = ''
  userLogedIn!: User | undefined

  constructor(private userService: UserService, private router: Router) {
    this.userLogedIn = this.userService.GetUserLogedIn()
  }

  ngOnInit() {
    this.userLogedIn = this.userService.GetUserLogedIn()
  }

  Logout() {
    this.userService.Logout()
    this.router.routeReuseStrategy.shouldReuseRoute = () => false
    this.router.onSameUrlNavigation = 'reload'
    this.router.navigate(['/trainer'])
  }
}
