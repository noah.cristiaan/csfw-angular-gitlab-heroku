import { Component,  OnInit } from '@angular/core'
import { FormControl, FormGroup, Validators } from '@angular/forms'
import { Router } from '@angular/router'
import { NgbModal } from '@ng-bootstrap/ng-bootstrap'
import { ItemService } from 'src/app/core/items/items.service'

@Component({
  selector: 'item-add',
  templateUrl: './item-add.component.html'
})
export class ItemAddComponent implements OnInit {
  addItemForm: FormGroup

  constructor(
    private router: Router,
    private modalService: NgbModal,
    private itemService: ItemService
  ) {
    this.addItemForm = new FormGroup({
        name: new FormControl('', [Validators.required, Validators.minLength(2)]),
        description: new  FormControl('', [Validators.required, Validators.minLength(2)]),
        imageItem: new  FormControl('', [Validators.required, Validators.minLength(2)])
    })
  }

  ngOnInit() {
  }

  onSubmit() {
    console.log(this.addItemForm.value)
    this.itemService.Add(this.addItemForm.value).subscribe()
    this.addItemForm.reset
    setTimeout(() => {
      this.router.navigate(['/pokedex'])
    }, 100)
    this.modalService.dismissAll()
  }
}
