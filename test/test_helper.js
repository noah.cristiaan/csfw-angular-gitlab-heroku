const mongoose = require('mongoose')

mongoose.connect('mongodb://localhost/pokenodemon')
mongoose.connection
    .once('open', () => console.log('Good to go!'))
    .on('error', (error) => {
        console.warn('Warning', error)
    })
    