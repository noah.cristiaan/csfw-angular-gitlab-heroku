import { Component, OnInit } from '@angular/core'
import { UserService } from 'src/app/core/user/user.service'
import { Pokemon, PokemonType } from '../pokemon/pokemon.model'
import { environment } from 'src/environments/environment'
import { User } from 'src/app/core/user/user.model'
import { PokemonService } from '../pokemon/pokemon.service'
import { Router } from '@angular/router'

@Component({
  selector: 'pokemon-trainer',
  templateUrl: './pokemon-trainer.component.html'
})
export class PokemonTrainerComponent implements OnInit {
  runningMode: string = ''
  apiUrl: string = ''
  version: string = ''
  userLogedIn: User | undefined
  pokemonTeam: Pokemon[] = []

  constructor(
    private userService: UserService,
    private pokemonService: PokemonService,
    private router: Router
  ) {

  }

  ngOnInit() {
    this.runningMode = environment.production ? 'production' : 'development'
    this.apiUrl = environment.apiUrl
    this.version = environment.version
    this.GetUserAndTrainer()
    this.pokemonTeam.length = 6
  }

  async GetUserAndTrainer() {
    if (this.userService.GetUserLogedIn() !== undefined) {
      this.userLogedIn = this.userService.GetUserLogedIn()
      this.pokemonTeam = this.userLogedIn!.pokemonTeam!
    }
  }

  DeletePokemonFromTeam(pokemon: Pokemon) {
    const index = this.pokemonTeam!.indexOf(pokemon, 0)
    if (index > -1) {
      this.pokemonTeam!.splice(index, 1)
      this.pokemonTeam!.length = 6
    }
    this.userService.Update(this.userLogedIn!).subscribe(() => {
      this.userService.Login(this.userLogedIn!)
      setTimeout(() => {
        this.router.navigate(['/trainer'])
      }, 100)
    })
  }
}
