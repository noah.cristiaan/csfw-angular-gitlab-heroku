import { HttpClientTestingModule } from '@angular/common/http/testing'
import { TestBed } from '@angular/core/testing'
import { Router } from '@angular/router'
import { RouterTestingModule } from '@angular/router/testing'
import { of } from 'rxjs'
import { Pokemon, PokemonType } from '../pokemon/pokemon.model'
import { PokemonService } from '../pokemon/pokemon.service'
import { PokedexComponent } from './pokedex.component'

const expectedPokemons: Pokemon[] = [
  {
    _id: '1',
    name: 'Charizard ',
    types: [PokemonType.FIRE, PokemonType.FLYING],
    moves: [],
    nature: 'Brave',
    ability: 'Blaze',
    pokemonImageURL: 'https://assets.pokemon.com/assets/cms2/img/pokedex/detail/006.png',
    item: undefined
  }
]
const expectedOtherPokemons: Pokemon[] = [
  {
    _id: '2',
    name: 'Blastoise ',
    types: [PokemonType.WATER],
    moves: [],
    nature: 'Brave',
    ability: 'Torrent',
    pokemonImageURL: 'https://assets.pokemon.com/assets/cms2/img/pokedex/detail/009.png',
    item: undefined
  }
]
//describe to start tests
describe('Pokedex', () => {
  //mock variables used in tests
  let component: PokedexComponent
  let pokemonService: jasmine.SpyObj<PokemonService>

  beforeEach(() => {
    //create mock from httpclient
    pokemonService = jasmine.createSpyObj('CarServiceService', ['Get', 'getCarAllOtherCars', 'deleteCar'])

    //configure object under test
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule] , 
      providers: [{ provide: PokemonService, useValue: pokemonService }]
    })
    //create object under test
    component = TestBed.createComponent(PokedexComponent).componentInstance

    //di for object under test
    pokemonService = TestBed.inject(PokemonService) as jasmine.SpyObj<PokemonService>
  })

  //test if service is created
  it('should be created', () => {
    //expect service to exist
    expect(component).toBeTruthy()
  })

  it('should have correct data', () => {
    pokemonService.Get.and.returnValue(of(expectedPokemons))
    pokemonService.Get.and.returnValue(of(expectedOtherPokemons))

    component.ngOnInit()
    component.GetPokedex()

    expect(component.pokedex!.length).toBeGreaterThan(0)
    expect(component.pokedex![0]._id).toEqual(expectedOtherPokemons[0]._id)
  })
})
