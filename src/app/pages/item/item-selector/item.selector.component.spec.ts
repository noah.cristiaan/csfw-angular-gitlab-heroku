import { HttpClientTestingModule } from '@angular/common/http/testing'
import { TestBed } from '@angular/core/testing'
import { RouterTestingModule } from '@angular/router/testing'
import { of } from 'rxjs'
import { User, UserGender, UserRole } from 'src/app/core/user/user.model'
import * as moment from 'moment'
import { UserService } from 'src/app/core/user/user.service'
import { Pokemon, PokemonType } from '../../pokemon/pokemon.model'
import { PokemonService } from '../../pokemon/pokemon.service'
import { PokemonItem } from 'src/app/core/items/items.model'
import { ItemSelectorComponent } from './item.selector.component'
import { ItemService } from 'src/app/core/items/items.service'

const expectedItem: PokemonItem[] = [
  {
    _id: '1',
    name: 'Amulet Coin',
    description: 'Doubles prize money if held.',
    imageURL: 'https://img.pokemondb.net/sprites/items/amulet-coin.png'
  }
]

const expectedUserWithPokemonTeam: User[] = [
  {
    _id: '1',
    birthdate: moment().toDate(),
    password: 'secret',
    firstname: 'Noah',
    lastname: 'de Keijzer',
    emailaddress: 'noah.cristiaan@gmail.com',
    userGender: UserGender.Male,
    userRole: UserRole.Guest,
    badges: 0,
    pokemonTeam: [
      {
        _id: '1',
        pokemonImageURL: 'https://assets.pokemon.com/assets/cms2/img/pokedex/detail/006.png',
        name: 'Charizard ',
        types: [PokemonType.FIRE, PokemonType.FLYING],
        moves: [],
        nature: 'Brave',
        ability: 'Blaze',
        item: '1'
      }
    ]
  }
]

const expectedPokemonsWithItem: Pokemon[] = [
  {
    _id: '1',
    pokemonImageURL: 'https://assets.pokemon.com/assets/cms2/img/pokedex/detail/006.png',
    name: 'Charizard ',
    types: [PokemonType.FIRE, PokemonType.FLYING],
    moves: ['1'],
    nature: 'Brave',
    ability: 'Blaze',
    item: '1'
  }
]
//describe to start tests

describe('Update item of pokemon in the pokemon team of the user', () => {
  //mock variables used in tests

  let component: ItemSelectorComponent
  let userServiceSpy: jasmine.SpyObj<UserService>
  let pokemonServiceSpy: jasmine.SpyObj<PokemonService>
  let pokemonItemServiceSpy: jasmine.SpyObj<ItemService>

  beforeEach(() => {
    //create mock from httpclient
    userServiceSpy = jasmine.createSpyObj('UserService', [
      'Get',
      'GetById',
      'Update',
      'GetUserLogedIn',
      'SetUserLogedIn'
    ])
    pokemonServiceSpy = jasmine.createSpyObj('PokemonService', [
      'Get',
      'GetById',
      'GetCurrentPokemon',
      'SetCurrentPokemon'
    ])
    pokemonItemServiceSpy = jasmine.createSpyObj('ItemService', ['Get', 'GetById'])

    //configure object under test
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule],
      providers: [
        { provide: PokemonService, useValue: pokemonServiceSpy },
        { provide: UserService, useValue: userServiceSpy },
        { provide: ItemService, useValue: pokemonItemServiceSpy }
      ]
    }).compileComponents()
    //create object under test
    component = TestBed.createComponent(ItemSelectorComponent).componentInstance

    //di for object under test
    userServiceSpy = TestBed.inject(UserService) as jasmine.SpyObj<UserService>
    pokemonServiceSpy = TestBed.inject(PokemonService) as jasmine.SpyObj<PokemonService>
    pokemonItemServiceSpy = TestBed.inject(ItemService) as jasmine.SpyObj<ItemService>

    pokemonServiceSpy.currentPokemon = expectedUserWithPokemonTeam[0].pokemonTeam[0]
    userServiceSpy.userLogedIn = expectedUserWithPokemonTeam[0]
    component.items = expectedItem
    component.userLogedIn = expectedUserWithPokemonTeam[0]
    component.pokemon = expectedUserWithPokemonTeam[0].pokemonTeam[0]
  })

  //test if service is created
  it('should be created', () => {
    //expect service to exist
    expect(component).toBeTruthy()
  })

  it('should have correct data', () => {
    component.ngOnInit()
    expect(component.userLogedIn?.pokemonTeam!.length).toBeGreaterThan(0)
  })

  it('should add item to pokemon in pokemon team', () => {
    component.ngOnInit()
    userServiceSpy.Update.and.returnValue(of(expectedUserWithPokemonTeam[0]))
    pokemonItemServiceSpy.Get.and.returnValue(of(expectedItem))
    pokemonServiceSpy.GetById.and.returnValue(of(expectedUserWithPokemonTeam[0].pokemonTeam[0]))
    component.AddItem(expectedItem[0]._id)
    expect(component.userLogedIn?.pokemonTeam![0].item).toEqual(expectedItem[0]._id)
  })
})
