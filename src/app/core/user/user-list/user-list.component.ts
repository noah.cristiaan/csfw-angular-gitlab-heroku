import { Component, OnInit } from '@angular/core'
import { User } from '../user.model'
import { environment } from 'src/environments/environment'
import { UserService } from '../user.service'
import { Observable } from 'rxjs'
import { ActivatedRoute, Router } from '@angular/router'
import { DashboardComponent } from '../../dashboard/dashboard.component'

@Component({
  selector: 'user-list',
  templateUrl: './user-list.component.html'
})
export class UserListComponent implements OnInit {
  runningMode: string = ''
  apiUrl: string = ''
  version: string = ''
  userLogedIn: User | undefined
  users: User[] | undefined

  constructor(
    private route: ActivatedRoute,
    private userService: UserService,
    private router: Router
  ) {}

  ngOnInit() {
    this.runningMode = environment.production ? 'production' : 'development'
    this.apiUrl = environment.apiUrl
    this.version = environment.version
    this.userService.Get().subscribe((results: User[]) => (this.users = results))
    this.userLogedIn = this.userService.GetUserLogedIn()
  }

  Delete(_id: String) {
    this.userService.GetById(_id).subscribe(async (result: User) => {
      this.userService.Delete(result, _id).subscribe(async () => {
        let currentUrl = this.router.url
        this.router.routeReuseStrategy.shouldReuseRoute = () => false
        this.router.onSameUrlNavigation = 'reload'
        this.router.navigate([currentUrl])
      })
    })
  }
}
