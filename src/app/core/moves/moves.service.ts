import { HttpClient } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { Observable, tap } from 'rxjs'
import { PokemonMove, StatusEffect } from 'src/app/core/moves/moves.model'
import { PokemonType } from 'src/app/pages/pokemon/pokemon.model'

@Injectable({
  providedIn: 'root'
})
export class PokemonMoveService {
  APIString: String = 'https://pokenodemon-api.herokuapp.com/'
  // APIString: String = 'http://localhost:8080/'

  constructor(public readonly http: HttpClient) {}

  Get(): Observable<PokemonMove[]> {
    return this.http.get(this.APIString + 'moves').pipe(tap(console.log))
  }

  GetById(id: String): Observable<PokemonMove> {
    return this.http.get(this.APIString + 'moves/' + id).pipe(tap(console.log))
  }

  Add(move: PokemonMove): Observable<PokemonMove> {
    return this.http.post<any>(this.APIString + 'moves', move).pipe(tap(console.log))
  }

  Update(move: PokemonMove): Observable<PokemonMove> {
    return this.http.put<PokemonMove>(this.APIString + 'moves/' + move._id, move).pipe(tap(console.log))
  }

  Delete(move: PokemonMove, moveId: String) {
    return this.http
      .delete<any>(this.APIString.toString() + 'moves/' + moveId)
      .pipe(tap(console.log))
      .subscribe()
  }
}
