import { Component, Injectable, OnInit } from '@angular/core'
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms'
import { Router } from '@angular/router'
import { NgbModal } from '@ng-bootstrap/ng-bootstrap'
import { StatusEffect } from 'src/app/core/moves/moves.model'
import { PokemonMoveService } from 'src/app/core/moves/moves.service'
import { TypeImageSelectorComponent } from 'src/app/pages/type/type-image-selector.component'
import { environment } from 'src/environments/environment'
import { Pokemon, PokemonType } from '../../pokemon/pokemon.model'
import { PokemonService } from '../../pokemon/pokemon.service'

@Component({
  selector: 'moves-add',
  templateUrl: './moves-add.component.html'
})
export class MovesAddComponent implements OnInit {
  pokemon!: Pokemon
  runningMode: string = ''
  apiUrl: string = ''
  version: string = ''
  pokemonMoveService!: PokemonMoveService
  pokemonTypes: String[]
  addMovesForm: FormGroup
  statusEffects: String[]

  constructor(
    pokemonMoveService: PokemonMoveService,
    readonly pokemonService: PokemonService,
    private router: Router,
    private modalService: NgbModal,
  ) {
    this.pokemonTypes = Object.keys(PokemonType)
    this.statusEffects = Object.keys(StatusEffect)

    this.pokemonMoveService = pokemonMoveService
    this.addMovesForm = new FormGroup({
        name: new FormControl('', [Validators.required, Validators.minLength(2)]),
        type: new FormControl('', Validators.required),
        statusEffect: new FormControl('', Validators.required),
        damage: new FormControl('', Validators.required),
        accuracy: new FormControl('', Validators.required)
    })
  }

  ngOnInit() {
    this.runningMode = environment.production ? 'production' : 'development'
    this.apiUrl = environment.apiUrl
    this.version = environment.version
  }
  onSubmit() {
    console.log(this.addMovesForm.value)
    this.pokemonMoveService.Add(this.addMovesForm.value).subscribe()
    this.addMovesForm.reset
    setTimeout(() => {
      this.router.navigate(['/pokedex'])
    }, 100)
    this.modalService.dismissAll()
  }
}
