import { Injectable } from '@angular/core'
import { map, Observable, tap } from 'rxjs'
import { UserGender, UserRole, User } from './user.model'
import { HttpClient } from '@angular/common/http'
import { noUndefined } from '@angular/compiler/src/util'

@Injectable({
  providedIn: 'root'
})
export class UserService {
  userLogedIn!: User | undefined
  APIString: String = 'https://pokenodemon-api.herokuapp.com/'
  // APIString: String = 'http://localhost:8080/'

  constructor(public readonly http: HttpClient) {}

  Get(): Observable<User[]> {
    return this.http.get(this.APIString + 'users').pipe(tap(console.log))
  }

  GetFollowing(_idCurrentUser: String): Observable<User[]> {
    return this.http.get(this.APIString + 'users/follow/' + _idCurrentUser).pipe(tap(console.log))
  }

  GetById(id: String): Observable<User> {
    return this.http.get(this.APIString + 'users/' + id).pipe(tap(console.log))
  }

  Add(user: User): Observable<User> {
    return this.http.post<any>(this.APIString.toString() + 'auth/register', user).pipe(tap(console.log))
  }

  Update(user: User): Observable<User> {
    return this.http.put<User>(this.APIString + 'users/' + user._id, user).pipe(tap(console.log))
  }

  GetByEmail(emailaddressGet: string): Observable<User> {
    return this.Get().pipe(
      map(
        (users: User[]) =>
          users.filter((users: { emailaddress: String }) => users.emailaddress === emailaddressGet)[0]
      )
    )
  }

  FollowUser(_idCurrentUser: String, _idUserToFollow: String, currentUser: User) {
    return this.http.post<any>(this.APIString + 'users/follow', { "currentUserId": _idCurrentUser, "userToFollowId": _idUserToFollow }).pipe(tap(console.log)).subscribe();
  }
  
  Delete(user: User, userId: String) {
    return this.http.delete<any>(this.APIString.toString() + 'users/' + userId).pipe(tap(console.log))
  }

  Login(user: User) {
    this.userLogedIn = user
    return this.http.post<any>(this.APIString.toString() + 'auth/login/', user).pipe(tap(console.log))

  }

  Logout() {
    this.userLogedIn = undefined
    console.log('Logout user:', this.userLogedIn)
  }
  GetUserLogedIn() {
    console.log('User loged in:', this.userLogedIn)
    return this.userLogedIn
  }
}
