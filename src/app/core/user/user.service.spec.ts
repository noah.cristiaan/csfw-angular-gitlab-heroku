import { HttpClient, HttpClientModule } from '@angular/common/http'
import { TestBed } from '@angular/core/testing'
import { HttpClientTestingModule } from '@angular/common/http/testing'

import { of } from 'rxjs'
import { User, UserGender, UserRole } from './user.model'
import { UserService } from './user.service'

const expectedUserData: User[] = [
  {
    _id: '1',
    firstname: 'Noah',
    lastname: 'de Keijzer',
    birthdate: new Date(),
    emailaddress: 'noah.cristiaan@gmail.com',
    userGender: UserGender.Male,
    userRole: UserRole.Admin,
    password: 'Hahamlknb1',
    pokemonTeam: [],
    badges: 0
  },
  {
    _id: '2',
    firstname: 'Deborah',
    lastname: 'van Selm',
    birthdate: new Date(),
    emailaddress: 'deborahvanselm@gmail.com',
    userGender: UserGender.Male,
    userRole: UserRole.Admin,
    password: 'IloveNoah123',
    pokemonTeam: [],
    badges: 0
  }
]

const updatedUserData: User = {
  _id: '2',
  firstname: 'Deborah',
  lastname: 'de Keijzer',
  birthdate: new Date(),
  emailaddress: 'de@gmail.com',
  userGender: UserGender.Male,
  userRole: UserRole.Admin,
  password: 'IloveNoah123',
  pokemonTeam: [],
  badges: 0
}

describe('UserService', () => {
  let userService: UserService
  let httpSpy: jasmine.SpyObj<HttpClient>

  beforeEach(() => {
    httpSpy = jasmine.createSpyObj('HttpClient', ['get', 'post', 'delete', 'put'])
    TestBed.configureTestingModule({
      providers: [{ provide: HttpClient, useValue: httpSpy }]
    })
    userService = TestBed.inject(UserService)

    httpSpy = TestBed.inject(HttpClient) as jasmine.SpyObj<HttpClient>
  })

  it('Should create the User Service', () => {
    expect(userService).toBeTruthy()
  })

  it('Should return a array with 2 users', (done: DoneFn) => {
    httpSpy.get.and.returnValue(of(expectedUserData))

    userService.Get().subscribe((users: User[]) => {
      console.log(users)
      expect(users.length).toBe(2)
      expect(users[0]._id).toEqual(expectedUserData[0]._id)
      done()
    })
  })

  it('Should return the user with _id 1', (done: DoneFn) => {
    httpSpy.get.and.returnValue(of(expectedUserData[0]))

    userService.GetById('1').subscribe((users: User) => {
      expect(users._id).toEqual(expectedUserData[0]._id)
      done()
    })
  })

  it('Should delete user with _id 1', (done: DoneFn) => {
    httpSpy.delete.and.returnValue(of(expectedUserData[0]))

    userService.Delete(expectedUserData[0], expectedUserData[0]._id).subscribe((users: User) => {
      expect(users._id).toEqual(expectedUserData[0]._id)
      done()
    })
  })

  it('Should create a user with _id 1', (done: DoneFn) => {
    httpSpy.post.and.returnValue(of(expectedUserData[0]))

    userService.Add(expectedUserData[0])
    expect(expectedUserData[0]._id).toEqual(expectedUserData[0]._id)
    done()
  })

  it('Should update the user with _id 2', (done: DoneFn) => {
    httpSpy.put.and.returnValue(of(updatedUserData))

    userService.Update(expectedUserData[1]).subscribe((users: User) => {
      expect(users._id).toEqual(updatedUserData._id)
      done()
    })
  })
})
