import { Component, Injectable, OnInit, ViewChild } from '@angular/core'
import { FormControl, FormGroup, Validators } from '@angular/forms'
import { ActivatedRoute, Router } from '@angular/router'
import { PokemonItem } from 'src/app/core/items/items.model'
import { ItemService } from 'src/app/core/items/items.service'
import { PokemonMove, StatusEffect } from 'src/app/core/moves/moves.model'
import { PokemonMoveService } from 'src/app/core/moves/moves.service'
import { User } from 'src/app/core/user/user.model'
import { UserService } from 'src/app/core/user/user.service'
import { environment } from 'src/environments/environment'
import { ItemComponent } from '../item/item-modal/item.modal.component'
import { MovesComponent } from '../moves/moves-modal/moves.modal.component'
import { Pokemon, PokemonType } from '../pokemon/pokemon.model'
import { PokemonService } from '../pokemon/pokemon.service'

@Component({
  selector: 'pokemon',
  templateUrl: './pokemon.component.html'
})
export class PokemonComponent implements OnInit {
  runningMode: string = ''
  apiUrl: string = ''
  version: string = ''
  pokemon!: Pokemon
  editPokemonForm: FormGroup
  pokemonType = PokemonType
  statusEffect = StatusEffect
  moves: PokemonMove[] = []
  movesIds: String[] = []
  item: PokemonItem | undefined
  userLogedIn!: User | undefined

  constructor(
    private router: Router,
    private pokemonService: PokemonService,
    private route: ActivatedRoute,
    private pokemonMoveService: PokemonMoveService,
    private itemsComponent: ItemComponent,
    private movesComponent: MovesComponent,
    private userService: UserService,
    private itemService: ItemService
  ) {
    this.editPokemonForm = new FormGroup({
      ability: new FormControl('', [Validators.required]),
      nature: new FormControl('', [Validators.required])
    })
  }

  ngOnInit() {
    this.runningMode = environment.production ? 'production' : 'development'
    this.apiUrl = environment.apiUrl
    this.version = environment.version
    this.moves.length = 4
    this.route.paramMap.subscribe(async (params) => {
      this.pokemonService.GetById(String(params.get('id'))).subscribe((result: Pokemon) => {
        this.pokemonService.SetCurrentPokemon(
          this.userService.GetUserLogedIn()?.pokemonTeam.find((pokemon) => pokemon._id === result._id)!
        )
        this.pokemon = this.pokemonService.GetCurrentPokemon()
        this.movesIds = this.pokemonService.GetCurrentPokemon().moves
        this.movesComponent.pokemon = this.pokemonService.GetCurrentPokemon()
        this.GetItemId()
        this.GetMoves()
      })
    })
  }

  async GetItemId() {
    this.GetItemObject(this.pokemonService.GetCurrentPokemon().item!)
  }

  GetMoves() {
    this.pokemon.moves.forEach((moveId) => {
      this.pokemonMoveService.GetById(moveId).subscribe((result) => {
        this.moves[this.pokemon.moves.indexOf(moveId)] = result
      })
    })
  }

  GetItemObject(itemId: String) {
    if (itemId) {
      this.itemService.GetById(itemId).subscribe((result) => {
        this.item = result
      })
    }
  }

  onSubmit() {
    console.log('onSubmit is aangeroepen')
    if (this.editPokemonForm.valid) {
      console.log('Form Submitted!', this.editPokemonForm.value)
      this.editPokemonForm.reset()
    }
  }
  openMoveLg() {
    this.movesComponent.openLg(this.pokemonService.GetCurrentPokemon())
  }
  openItemLg() {
    this.itemsComponent.openLg(this.pokemonService.GetCurrentPokemon())
  }

  DeleteMove(_idMove: String, _idPokemon: String) {
    // Pokemon Trainer
    var user = this.userService.GetUserLogedIn()
    var currentPokemon = this.pokemonService.GetCurrentPokemon()
    // Get move
    this.pokemonMoveService.GetById(_idMove).subscribe((result) => {
      // Find first compatible pokemon
      // Set pokemon apart
      var pokemonToRemoveMoveFrom = user!.pokemonTeam!.find((pokemon) => pokemon._id === currentPokemon._id)
      // Find first compatible move with pokemon moveset
      const index = pokemonToRemoveMoveFrom?.moves.indexOf(result._id, 0)
      if (index! > -1) {
        pokemonToRemoveMoveFrom?.moves.splice(index!, 1)
      }
      // Update pokemon in the team
      user!.pokemonTeam!.find((pokemon) => pokemon._id === pokemonToRemoveMoveFrom?._id)
      this.userService.Update(user!).subscribe(() => {
        this.userService.Login(user!)
      })
      this.UpdateComponent()
    })
  }
  AddItemToPokemon(id: string) {
    var user = this.userService.GetUserLogedIn()
    var currentPokemon = this.pokemonService.GetCurrentPokemon()
    var pokemonToRemoveItemFrom = user!.pokemonTeam!.find((pokemon) => pokemon._id === currentPokemon._id)

    this.itemService.GetById(id).subscribe((item) => {
      pokemonToRemoveItemFrom!.item! = item._id
    })
    this.UpdateComponent()
  }

  RemoveItemFromPokemon(id: String) {
    var user = this.userService.GetUserLogedIn()
    var currentPokemon = this.pokemonService.GetCurrentPokemon()
    var pokemonToRemoveItemFromInTeam = user!.pokemonTeam!.find(
      (pokemon) => pokemon._id === currentPokemon._id
    )

    this.itemService.GetById(id).subscribe((item) => {
      pokemonToRemoveItemFromInTeam!.item = undefined
    })
    this.UpdateComponent()
  }
  UpdateComponent() {
    let currentUrl = this.router.url
    this.router.routeReuseStrategy.shouldReuseRoute = () => false
    this.router.onSameUrlNavigation = 'reload'
    this.router.navigate([currentUrl])
  }
}
