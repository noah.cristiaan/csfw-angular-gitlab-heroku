import { ConstantPool } from '@angular/compiler'
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast'
import { Component, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { User } from 'src/app/core/user/user.model'
import { UserService } from 'src/app/core/user/user.service'
import { environment } from 'src/environments/environment'
import { NotLogedInComponent } from '../error/not-loged-in.modal'
import { Pokemon, PokemonType } from '../pokemon/pokemon.model'
import { PokemonService } from '../pokemon/pokemon.service'

@Component({
  selector: 'pokedex',
  templateUrl: './pokedex.component.html'
})
export class PokedexComponent implements OnInit {
  runningMode: string = ''
  apiUrl: string = ''
  version: string = ''
  userLogedIn!: User | undefined
  pokedex: Pokemon[] | undefined
  pokemonType = PokemonType
  addToTeamComponent!: NotLogedInComponent

  constructor(
    private pokemonService: PokemonService,
    addToTeamComponent: NotLogedInComponent,
    private userService: UserService,
    private router: Router
  ) {
    this.addToTeamComponent = addToTeamComponent
  }

  ngOnInit() {
    this.runningMode = environment.production ? 'production' : 'development'
    this.apiUrl = environment.apiUrl
    this.version = environment.version
    this.userLogedIn = this.userService.GetUserLogedIn()
    this.GetPokedex()
  }

  GetPokedex(){
    this.pokemonService.Get().subscribe((results: Pokemon[]) => (this.pokedex = results))
  }
  
  async AddToTeam(id: String) {
    var numberInTeam = 0
    this.userLogedIn!.pokemonTeam!.forEach((pokemon) => {
      if (pokemon != null) {
        numberInTeam++
      }
    })
    if (this.userLogedIn === undefined) {
      this.addToTeamComponent.openSmFailed()
    } else if (numberInTeam >= 6) {
      this.addToTeamComponent.openSmTeamFull()
    } else if (numberInTeam >= 0 && numberInTeam < 6) {
      this.pokemonService.GetById(id).subscribe((result) => {
        this.userLogedIn!.pokemonTeam[numberInTeam] = result
        this.userService.Update(this.userLogedIn!).subscribe()
      })
    }
    setTimeout(() => {
      this.router.navigate(['/trainer'])
    }, 100)
  }

  Delete(id: String) {
    this.pokemonService.Delete(id)
  }

  UpdateUser(user: User) {
    this.userService.Update(user!).subscribe((updatedUser) => {
      this.userService.Login(updatedUser)
    })
  }
}
