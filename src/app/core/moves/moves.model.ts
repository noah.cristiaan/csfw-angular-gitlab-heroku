import { PokemonType } from 'src/app/pages/pokemon/pokemon.model'

export enum StatusEffect {
  BURN = 'BURN',
  FREEZE = 'FREEZE',
  PARALYSIS = 'PARALYSIS',
  POISON = 'POISON',
  SLEEP = 'SLEEP',
  NONE = 'NONE' 
}

export class PokemonMove {
  _id: String
  name: string
  type: PokemonType
  statusEffect: StatusEffect
  accuracy: Number
  damage: Number

  constructor(
    _id: String,
    name: string,
    type: PokemonType,
    statusEffect: StatusEffect,
    accuracy: Number,
    damage: Number
  ) {
    this._id = _id
    this.name = name
    this.type = type
    this.statusEffect = statusEffect
    this.accuracy = accuracy
    this.damage = damage
  }
}
