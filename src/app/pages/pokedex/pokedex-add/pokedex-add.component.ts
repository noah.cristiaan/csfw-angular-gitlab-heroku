import { Component, Injectable, OnInit } from '@angular/core'
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms'
import { Router } from '@angular/router'
import { NgbModal } from '@ng-bootstrap/ng-bootstrap'
import { PokemonMoveService } from 'src/app/core/moves/moves.service'
import { TypeImageSelectorComponent } from 'src/app/pages/type/type-image-selector.component'
import { environment } from 'src/environments/environment'
import { Pokemon, PokemonType } from '../../pokemon/pokemon.model'
import { PokemonService } from '../../pokemon/pokemon.service'
import { PokedexComponent } from '../pokedex.component'
import { PokedexAddComponentModal } from './pokedex-add-modal/pokedex-add.modal.component'

@Component({
  selector: 'pokedex-add',
  templateUrl: './pokedex-add.component.html'
})
export class PokedexAddComponent implements OnInit {
  pokemon!: Pokemon
  runningMode: string = ''
  apiUrl: string = ''
  version: string = ''
  pokemonTypes: String[]
  addPokemonForm: FormGroup

  constructor(
    private pokemonMoveService: PokemonMoveService,
    private pokemonService: PokemonService,
    private router: Router,
    private modalService: NgbModal
  ) {
    this.pokemonTypes = Object.keys(PokemonType)
    this.pokemonMoveService = pokemonMoveService
    this.addPokemonForm = new FormGroup({
      pokemonImageURL: new FormControl('', Validators.required),
      name: new FormControl('', [Validators.required, Validators.minLength(2)]),
      type1: new FormControl('', Validators.required),
      type2: new FormControl(''),
      ability: new FormControl('', Validators.required),
      nature: new FormControl('', Validators.required)
    })
  }

  ngOnInit() {
    this.runningMode = environment.production ? 'production' : 'development'
    this.apiUrl = environment.apiUrl
    this.version = environment.version
  }
  onSubmit() {
    console.log(this.addPokemonForm.value)
    if (
      this.addPokemonForm.controls['type1'].value == this.addPokemonForm.controls['type2'].value ||
      this.addPokemonForm.controls['type2'].value == ''
    ) {
      this.pokemon = {
        _id: undefined,
        pokemonImageURL: this.addPokemonForm.controls['pokemonImageURL'].value,
        name: this.addPokemonForm.controls['name'].value,
        types: [this.addPokemonForm.controls['type1'].value],
        ability: this.addPokemonForm.controls['ability'].value,
        nature: this.addPokemonForm.controls['nature'].value,
        moves: [],
        item: undefined
      }
    } else {
      this.pokemon = {
        _id: undefined,
        pokemonImageURL: this.addPokemonForm.controls['pokemonImageURL'].value,
        name: this.addPokemonForm.controls['name'].value,
        types: [this.addPokemonForm.controls['type1'].value, this.addPokemonForm.controls['type2'].value],
        ability: this.addPokemonForm.controls['ability'].value,
        nature: this.addPokemonForm.controls['nature'].value,
        moves: [],
        item: undefined
      }
    }
    console.log(this.pokemon)
    this.pokemonService.Add(this.pokemon).subscribe(() => {
      this.router.routeReuseStrategy.shouldReuseRoute = () => false
      this.router.onSameUrlNavigation = 'reload'
      this.router.navigate(['/pokedex'])
      this.modalService.dismissAll()
    })
  }
}
