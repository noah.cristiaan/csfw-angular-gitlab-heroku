import { HttpClientTestingModule } from '@angular/common/http/testing'
import { TestBed } from '@angular/core/testing'
import { RouterTestingModule } from '@angular/router/testing'
import { of } from 'rxjs'
import { PokemonMoveService } from 'src/app/core/moves/moves.service'
import { User, UserGender, UserRole } from 'src/app/core/user/user.model'
import * as moment from 'moment'
import { UserService } from 'src/app/core/user/user.service'
import { UserEditComponent } from './user-edit.component'

const userOld: User[] = [
  {
    _id: '1',
    birthdate: moment().toDate(),
    password: 'secret',
    firstname: 'Noah',
    lastname: 'de Keijzer',
    emailaddress: 'noah.cristiaan@gmail.com',
    userGender: UserGender.Male,
    userRole: UserRole.Guest,
    badges: 0,
    pokemonTeam: []
  }
]

const userNew: User = {
  _id: '1',
  birthdate: moment().toDate(),
  password: 'secret',
  firstname: 'Noah',
  lastname: 'de Keijzer',
  emailaddress: 'noahdekeijzer@gmail.com',
  userGender: UserGender.Male,
  userRole: UserRole.Guest,
  badges: 0,
  pokemonTeam: []
}

//describe to start tests
describe('Update user', () => {
  //mock variables used in tests
  let userServiceSpy: jasmine.SpyObj<UserService>
  let component: UserEditComponent

  beforeEach(() => {
    //create mock from httpclient
    userServiceSpy = jasmine.createSpyObj('UserService', ['Get', 'GetById', 'Update'])

    //configure object under test
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule],
      providers: [{ provide: UserService, useValue: userServiceSpy }]
    }).compileComponents()
    //create object under test
    component = TestBed.createComponent(UserEditComponent).componentInstance
    component.user = userOld[0]
    component.editUserForm.setValue({
      firstname: userNew.firstname,
      lastname: userNew.lastname,
      emailaddress: userNew.emailaddress,
      birthdate: userNew.birthdate,
      userGender: userNew.userGender
    })
    //di for object under test
    userServiceSpy = TestBed.inject(UserService) as jasmine.SpyObj<UserService>
  })

  //test if service is created
  it('should be created', () => {
    //expect service to exist
    expect(component).toBeTruthy()
  })

  it('should have correct data', () => {
    component.ngOnInit()
    expect(component.user).toBeTruthy()
  })

  it('should update data of user', () => {
    component.ngOnInit()
    userServiceSpy.Update.and.returnValue(of(userNew))
    component.onSubmit(userOld[0]._id)
    expect(component.user).toEqual(userNew)
  })
})
