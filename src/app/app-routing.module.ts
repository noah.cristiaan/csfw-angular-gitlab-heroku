import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { DashboardComponent } from './core/dashboard/dashboard.component'
import { LayoutComponent } from './core/layout/layout.component'
import { UserDetailComponent } from './core/user/user-detail/user-detail.component'
import { UserListComponent } from './core/user/user-list/user-list.component'
import { AboutComponent } from './pages/about/about.component'
import { UserEditComponent } from './core/user/user-edit/user-edit.component'
import { UserDefaultComponent } from './core/user/user-default/user-default.component'
import { UserRegisterComponent } from './core/user/user-register/user-register.component'
import { PokedexComponent } from './pages/pokedex/pokedex.component'
import { PokemonTrainerComponent } from './pages/pokemon-trainer/pokemon-trainer.component'
import { PokemonComponent } from './pages/pokemon/pokemon.component'
import { PokemonTrainerDashboardComponent } from './pages/pokemon-trainer/pokemon-trainer-dashboard/pokemon-trainer-dashboard.component'
import { PokedexAddComponentModal } from './pages/pokedex/pokedex-add/pokedex-add-modal/pokedex-add.modal.component'
import { UserLoginComponent } from './core/user/user-login/user-login.component'
import { MovesAddComponentModal } from './pages/moves/moves-add/moves-add-modal/moves-add.modal.component'
import { ItemAddComponentModal } from './pages/item/item-add/item-add-modal/item-add.modal.component'
import { UserFollowingComponent } from './core/user/user-following/user-following.component'

const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      { path: '', pathMatch: 'full', redirectTo: 'dashboard' },
      {
        path: 'dashboard',
        component: DashboardComponent,
        children: [
          { path: '', component: UserDefaultComponent },
          { path: 'users', component: UserListComponent },
          { path: 'register', component: UserRegisterComponent },
          { path: ':id', component: UserDetailComponent },
          { path: ':id/edit', component: UserEditComponent }
        ]
      },
      {
        path: 'following',
        component: UserFollowingComponent,
        children: [
          {
            path: ':id/details',
            component: PokemonComponent,
            children: [
              {
                path: 'pokemon',
                component: PokemonComponent,
                children: [{ path: ':id/edit', component: PokemonComponent }]
              }
            ]
          }
        ]
      },
      {
        path: 'trainer',
        component: PokemonTrainerDashboardComponent,
        children: [
          { path: '', component: PokemonTrainerComponent },
          { path: 'pokemon/:id/edit', component: PokemonComponent }
        ]
      },
      {
        path: 'pokedex',
        component: PokedexComponent,
        children: [
          { path: 'addPokemon', component: PokedexAddComponentModal },
          { path: 'addMove', component: MovesAddComponentModal },
          { path: 'addItem', component: ItemAddComponentModal }
        ]
      },
      { path: 'login', component: UserLoginComponent },
      { path: 'register', component: UserRegisterComponent },
      { path: 'about', component: AboutComponent },
      { path: 'user-list', component: UserListComponent },
      { path: 'user-detail/:id', component: UserDetailComponent }
    ]
  },
  { path: '**', redirectTo: '/' }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
