import { Component, OnInit } from '@angular/core'
import { User } from '../user.model'
import { environment } from 'src/environments/environment'
import { UserService } from '../user.service'
import { ActivatedRoute, Router } from '@angular/router'
import { UserGender } from '../user.model'

@Component({
  selector: 'user-detail',
  templateUrl: './user-detail.component.html'
})
export class UserDetailComponent implements OnInit {
  runningMode: string = ''
  apiUrl: string = ''
  version: string = ''
  user!: User | undefined
  userGender = UserGender
  userLogedIn: User | undefined
  usersFollowing: User[] | undefined

  constructor(private route: ActivatedRoute, private userService: UserService, private router: Router) {
    this.userLogedIn = this.userService.GetUserLogedIn()
    this.route.paramMap.subscribe((params) => {
      userService.GetById(String(params.get('id'))).subscribe((result: User) => (this.user = result))
    })
    userService.GetFollowing(this.userLogedIn?._id!).subscribe((results: User[]) =>{
      this.usersFollowing = results
    })
  }

  ngOnInit() {
    this.runningMode = environment.production ? 'production' : 'development'
    this.apiUrl = environment.apiUrl
    this.version = environment.version
  }

  FollowUser(_idUserToFollow: String) {
    this.userService.FollowUser(this.userLogedIn?._id!, _idUserToFollow, this.userLogedIn!)
    let currentUrl = this.router.url
    this.router.routeReuseStrategy.shouldReuseRoute = () => false
    this.router.onSameUrlNavigation = 'reload'
    this.router.navigate([currentUrl])
  }

  SeeFollowing() {
    this.router.navigate(['following'])
  }

  displayCondition(userDetailId: String) {
    let userSearch = this.usersFollowing!.find(
      (user) => user._id === userDetailId
    )
    if(userSearch && userSearch._id){
      return true
    }
    return false
  }
}
