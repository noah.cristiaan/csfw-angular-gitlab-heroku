import { Component, OnInit } from '@angular/core'
import { User } from '../user.model'
import { environment } from 'src/environments/environment'
import { UserService } from '../user.service'
import { FormControl, FormGroup, Validators } from '@angular/forms'
import { Router } from '@angular/router'

@Component({
  selector: 'user-login',
  templateUrl: './user-login.component.html'
})
export class UserLoginComponent implements OnInit {
  runningMode: string = ''
  apiUrl: string = ''
  version: string = ''
  userLogedIn!: User | undefined
  loginUserForm!: FormGroup

  constructor(
    private userService: UserService,
    private router: Router
  ) {
    this.userLogedIn = userService.GetUserLogedIn()
  }

  ngOnInit() {
    this.runningMode = environment.production ? 'production' : 'development'
    this.apiUrl = environment.apiUrl
    this.version = environment.version

    this.loginUserForm = new FormGroup({
      emailaddress: new FormControl('', [Validators.email, Validators.required]),
      password: new FormControl('', Validators.required)
    })
    this.userLogedIn = this.userService.GetUserLogedIn()
  }
  async onSubmit() {
    console.log('onSubmit', this.loginUserForm.value)
    // Check if form is invalid

    if (this.loginUserForm.invalid) {
      return
    } else {
      // Check if user exists
      this.userService
        .GetByEmail(this.loginUserForm.controls['emailaddress'].value)
        .subscribe(async (result: User) => {
          if (!result) {
            this.loginUserForm.reset
          } else {
            // Login user
            this.userService.Login(result)
            this.userLogedIn = this.userService.GetUserLogedIn()
            let currentUrl = this.router.url
            this.router.routeReuseStrategy.shouldReuseRoute = () => false
            this.router.onSameUrlNavigation = 'reload'
            this.router.navigate([currentUrl])
          }
        })
    }
  }
}
