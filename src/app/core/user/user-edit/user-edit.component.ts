import { Component, OnInit } from '@angular/core'
import { User } from '../user.model'
import { environment } from 'src/environments/environment'
import { UserService } from '../user.service'
import { ActivatedRoute, Router } from '@angular/router'
import { FormControl, FormGroup, Validators } from '@angular/forms'
import { UserGender } from '../user.model'

@Component({
  selector: 'user-edit',
  templateUrl: './user-edit.component.html'
})
export class UserEditComponent implements OnInit {
  runningMode: string = ''
  apiUrl: string = ''
  version: string = ''
  user!: User
  UserGender = UserGender
  editUserForm: FormGroup

  constructor(private route: ActivatedRoute, private userService: UserService, private router: Router) {
    this.route.paramMap.subscribe((params) => {
      userService.GetById(String(params.get('id'))).subscribe((result: User) => (this.user = result))
    })
    this.editUserForm = new FormGroup({
      firstname: new FormControl('', [Validators.required, Validators.minLength(2)]),
      lastname: new FormControl('', [Validators.required, Validators.minLength(2)]),
      emailaddress: new FormControl('', [Validators.email, Validators.required]),
      birthdate: new FormControl('', Validators.required),
      userGender: new FormControl('', Validators.required)
    })
  }

  ngOnInit() {
    this.runningMode = environment.production ? 'production' : 'development'
    this.apiUrl = environment.apiUrl
    this.version = environment.version
  }
  onSubmit(userId: String) {
    let userUpdated: User = this.editUserForm.value
    userUpdated._id = userId
    console.log('onSubmit')
    this.userService.Update(this.editUserForm.value).subscribe((result) =>{
      this.editUserForm.reset
      this.user = result
      setTimeout(() => {
        this.router.navigate(['/'])
      }, 100)
    })
  }
}
