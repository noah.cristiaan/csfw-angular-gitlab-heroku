import { HttpClient } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { Router } from '@angular/router'
import { Observable, tap } from 'rxjs'
import { PokemonMove } from 'src/app/core/moves/moves.model'
import { PokemonMoveService } from 'src/app/core/moves/moves.service'
import { Pokemon, PokemonType } from './pokemon.model'

@Injectable({
  providedIn: 'root'
})
export class PokemonService {
  APIString: String = 'https://pokenodemon-api.herokuapp.com/'
  // APIString: String = 'http://localhost:8080/'
  readonly pokemonMoves!: PokemonMove[]
  public currentPokemon!: Pokemon

  constructor(public readonly http: HttpClient) {}

  Get(): Observable<Pokemon[]> {
    return this.http.get(this.APIString + 'pokemons').pipe(tap(console.log))
  }

  GetById(_id: String): Observable<Pokemon> {
    return this.http.get(this.APIString + 'pokemons/' + _id).pipe(tap(console.log))
  }

  Add(pokemon: Pokemon): Observable<Pokemon> {
    return this.http.post<any>(this.APIString + 'pokemons', pokemon).pipe(tap(console.log))
  }

  Delete(id: String) {
    this.http
      .delete<any>(this.APIString + 'pokemons/' + id)
      .pipe(tap(console.log))
      .subscribe()
  }

  AddMove(move: PokemonMove) {}

  GetCurrentPokemon() {
    return this.currentPokemon
  }

  SetCurrentPokemon(pokemon: Pokemon) {
    this.currentPokemon = pokemon
  }
}
