import { Component, OnInit } from '@angular/core'
import { User } from '../user.model'
import { environment } from 'src/environments/environment'
import { UserService } from '../user.service'
import { Observable } from 'rxjs'
import { ActivatedRoute, Router } from '@angular/router'
import { DashboardComponent } from '../../dashboard/dashboard.component'
import { Pokemon } from 'src/app/pages/pokemon/pokemon.model'

@Component({
  selector: 'user-following',
  templateUrl: './user-following.component.html'
})
export class UserFollowingComponent implements OnInit {
  runningMode: string = ''
  apiUrl: string = ''
  version: string = ''
  userLogedIn: User | undefined
  userClicked: User | undefined
  pokemonTeam!: Pokemon[]
  usersFollowing: User[] = []

  constructor(private route: ActivatedRoute, private userService: UserService, private router: Router) {
    this.userClicked = undefined
    this.userLogedIn = this.userService.GetUserLogedIn()
  }

  async ngOnInit() {
    this.userLogedIn = this.userService.GetUserLogedIn()
    this.userService.GetFollowing(this.userLogedIn?._id!).subscribe((results: User[]) =>{
      this.usersFollowing = results
    })
  }

  onClickUser(userClickedId: String) {
    this.userService.GetById(userClickedId).subscribe((result) => {
      this.userClicked = result
      this.pokemonTeam = this.userClicked.pokemonTeam
    })
  }
}
