import { Component, Injectable, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { NgbModal } from '@ng-bootstrap/ng-bootstrap'
import { PokemonItem } from 'src/app/core/items/items.model'
import { ItemService } from 'src/app/core/items/items.service'
import { User } from 'src/app/core/user/user.model'
import { UserService } from 'src/app/core/user/user.service'
import { environment } from 'src/environments/environment'
import { Pokemon, PokemonType } from '../../pokemon/pokemon.model'
import { PokemonService } from '../../pokemon/pokemon.service'

@Component({
  selector: 'item-selector',
  templateUrl: './item.selector.component.html'
})
@Injectable({
  providedIn: 'root'
})
export class ItemSelectorComponent implements OnInit {
  runningMode: string = ''
  apiUrl: string = ''
  version: string = ''
  items!: PokemonItem[]
  pokemonType = PokemonType
  pokemon!: Pokemon
  userLogedIn: User | undefined

  constructor(
    private router: Router,
    private userService: UserService,
    private pokemonService: PokemonService,
    private modalService: NgbModal,
    private itemService: ItemService
  ) {}

  ngOnInit() {
    this.runningMode = environment.production ? 'production' : 'development'
    this.apiUrl = environment.apiUrl
    this.version = environment.version
    if (!this.userLogedIn) {
      this.GetUserLogedIn()
    }
    if (!this.pokemon) {
      this.GetCurrentPokemon()
    }
    if (!this.items) {
      this.GetAllItems()
    }
  }

  GetAllItems() {
    this.itemService.Get().subscribe((results) => {
      this.items = results
    })
  }

  GetCurrentPokemon() {
    this.pokemon = this.pokemonService.GetCurrentPokemon()
  }

  GetUserLogedIn() {
    this.userLogedIn = this.userService.GetUserLogedIn()
  }

  AddItem(_idItem: String) {
    let pokemon = this.GetPokemonInTeam()
    pokemon!.item = _idItem
    this.UpdateUserPokemonTeam(pokemon)
  }

  GetPokemonInTeam(): Pokemon {
    var pokemonToAddMoveTo = this.userLogedIn!.pokemonTeam!.find(
      (pokemon) => pokemon._id === this.pokemon._id
    )
    return pokemonToAddMoveTo!
  }

  UpdateUserPokemonTeam(pokemon: Pokemon) {
    this.userService.Update(this.userLogedIn!).subscribe(() => {
      this.pokemonService.SetCurrentPokemon(pokemon!)
      this.modalService.dismissAll()
      this.UpdateComponent()
    })
  }
  UpdateComponent() {
    let currentUrl = this.router.url
    this.router.routeReuseStrategy.shouldReuseRoute = () => false
    this.router.onSameUrlNavigation = 'reload'
    this.router.navigate([currentUrl])
  }
}
