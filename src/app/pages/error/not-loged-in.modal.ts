import { Component, Injectable, OnInit, ViewEncapsulation } from '@angular/core'
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap'
import { environment } from 'src/environments/environment'

@Component({
  template: `
    <div class="modal-header">
      <h4 class="modal-title">Hi there!</h4>
      <button type="button" class="close" aria-label="Close" (click)="activeModal.dismiss('Cross click')">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      <p>Pokémon succesfully added to your team!</p>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-outline-dark" (click)="activeModal.close('Close click')">
        Close
      </button>
    </div>
  `
})
export class Success {
  constructor(private modalService: NgbModal, public activeModal: NgbActiveModal) {}

  open() {
    this.modalService.open(NotLogedInComponent, {
      size: 'sm'
    })
  }
}
@Component({
  template: `
    <div class="modal-header">
      <h4 class="modal-title">Hi there!</h4>
      <button type="button" class="close" aria-label="Close" (click)="activeModal.dismiss('Cross click')">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      <p>Please Login to be able to add the Pokémon to your team</p>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-outline-dark" (click)="activeModal.close('Close click')">
        Close
      </button>
    </div>
  `
})
export class Failed {
  constructor(private modalService: NgbModal, public activeModal: NgbActiveModal) {}

  open() {
    this.modalService.open(NotLogedInComponent, {
      size: 'sm'
    })
  }
}
@Component({
  template: `
    <div class="modal-header">
      <h4 class="modal-title">Hi there!</h4>
      <button type="button" class="close" aria-label="Close" (click)="activeModal.dismiss('Cross click')">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      <p>Your Pokemon Team is already full!</p>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-outline-dark" (click)="activeModal.close('Close click')">
        Close
      </button>
    </div>
  `
})
export class Full {
  constructor(private modalService: NgbModal, public activeModal: NgbActiveModal) {}

  open() {
    this.modalService.open(NotLogedInComponent, {
      size: 'sm'
    })
  }
}
Component({
  selector: 'not-loged-in',
  templateUrl: './not-loged-in.modal.html',
  encapsulation: ViewEncapsulation.None,
  styles: [
    `
        .dark-modal .modal-content {
          background-color: #292b2c;
          color: white;
        }
        .dark-modal .close {
          color: white;
        }
        .light-blue-backdrop {
          background-color: #5cb3fd;
        }
      `
  ]
})
@Injectable({
  providedIn: 'root'
})
export class NotLogedInComponent implements OnInit {
  runningMode: string = ''
  apiUrl: string = ''
  version: string = ''

  constructor(private modalService: NgbModal) {}

  ngOnInit() {
    this.runningMode = environment.production ? 'production' : 'development'
    this.apiUrl = environment.apiUrl
    this.version = environment.version
  }

  openSmSuccess() {
    this.modalService.open(Success, { size: 'sm' })
  }
  openSmFailed() {
    this.modalService.open(Failed, { size: 'sm' })
  }
  openSmTeamFull(){
    this.modalService.open(Full, { size: 'sm' })
  }
}
