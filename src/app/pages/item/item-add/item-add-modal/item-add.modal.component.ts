import { Component, Injectable, OnInit, ViewEncapsulation } from '@angular/core'
import { NgbModal } from '@ng-bootstrap/ng-bootstrap'
import { ItemService } from 'src/app/core/items/items.service'
import { environment } from 'src/environments/environment'
import { ItemAddComponent } from '../item-add.component'

@Component({
  selector: 'item-add',
  templateUrl: './item-add.modal.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: [
    `
      .dark-modal .modal-content {
        background-color: #292b2c;
        color: white;
      }
      .dark-modal .close {
        color: white;
      }
      .light-blue-backdrop {
        background-color: #5cb3fd;
      }
    `
  ]
})
export class ItemAddComponentModal implements OnInit {
  runningMode: string = ''
  apiUrl: string = ''
  version: string = ''

  constructor(
    private modalService: NgbModal,
    private itemService: ItemService,
  ) {}

  ngOnInit() {
    this.runningMode = environment.production ? 'production' : 'development'
    this.apiUrl = environment.apiUrl
    this.version = environment.version
    this.openLg()
  }

  openLg() {
    this.modalService.open(ItemAddComponent, { size: 'lg' })
  }

  close(){
    this.modalService.dismissAll()
  }
}
