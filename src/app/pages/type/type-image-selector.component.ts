import { THIS_EXPR } from '@angular/compiler/src/output/output_ast'
import { Component, Injectable, Input, OnInit } from '@angular/core'
import { PokemonMove } from 'src/app/core/moves/moves.model'
import { PokemonMoveService } from 'src/app/core/moves/moves.service'
import { PokemonType } from '../pokemon/pokemon.model'

@Component({
  selector: 'type-image-selector',
  templateUrl: './type-image-selector.component.html'
})
@Injectable({
  providedIn: 'root'
})
export class TypeImageSelectorComponent implements OnInit {
  pokemonType = PokemonType
  @Input() currentType: PokemonType | undefined
  move!: PokemonMove

  constructor(private readonly movesServices: PokemonMoveService) {
    this.currentType = this.currentType
  }

  ngOnInit() {}
}
