// import { TestBed, async } from '@angular/core/testing'
// import { RouterTestingModule } from '@angular/router/testing'
// import { AppComponent } from './app.component'
// import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
// import { DashboardComponent } from '../dashboard/dashboard.component'
// import { NavbarComponent } from '../navbar/navbar.component'
// import { AboutComponent } from 'src/app/pages/about/about.component'
// import { UsecaseComponent } from 'src/app/pages/about/usecases/usecase.component'
// import { LayoutComponent } from '../layout/layout.component'
// import { FooterComponent } from '../footer/footer.component'
// import { UserListComponent } from '../user/user-list/user-list.component'
// import { PokedexComponent } from 'src/app/pages/pokedex/pokedex.component'
// import { PokemonTrainerDashboardComponent } from 'src/app/pages/pokemon-trainer/pokemon-trainer-dashboard/pokemon-trainer-dashboard.component'
// import { PokemonTrainerComponent } from 'src/app/pages/pokemon-trainer/pokemon-trainer.component'
// import { PokemonComponent } from 'src/app/pages/pokemon/pokemon.component'
// import { UserDefaultComponent } from '../user/user-default/user-default.component'
// import { UserDetailComponent } from '../user/user-detail/user-detail.component'
// import { UserEditComponent } from '../user/user-edit/user-edit.component'
// import { UserLoginComponent } from '../user/user-login/user-login.component'
// import { UserRegisterComponent } from '../user/user-register/user-register.component'
// import { UserGender, UserMongoose, UserRole } from '../user/user.model'

// describe('AppComponent', () => {
//   beforeEach(async(() => {
//     TestBed.configureTestingModule({
//       imports: [RouterTestingModule, NgbModule],
//       declarations: [
//         AppComponent,
//         NavbarComponent,
//         LayoutComponent,
//         DashboardComponent,
//         FooterComponent,
//         AboutComponent,
//         UsecaseComponent,
//         UserListComponent,
//         UserDetailComponent,
//         UserEditComponent,
//         UserDefaultComponent,
//         UserRegisterComponent,
//         UserLoginComponent,
//         PokedexComponent,
//         PokemonTrainerComponent,
//         PokemonComponent,
//         PokemonTrainerDashboardComponent,
//         PokemonTrainerComponent
//       ]
//     }).compileComponents()
//   }))

//   it('should create the app', () => {
//     const fixture = TestBed.createComponent(AppComponent)
//     const app = fixture.debugElement.componentInstance
//     expect(app).toBeTruthy()
//   })

//   it(`should have as title 'PokeNodeMon'`, () => {
//     const fixture = TestBed.createComponent(AppComponent)
//     const app = fixture.debugElement.componentInstance
//     expect(app.title).toEqual('PokeNodeMon')
//   })
// })
